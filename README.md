# Microservice .Net

## In folder Play.Infra run this command

```bash
docker run -d --rm --name mongo -p 27017:27017 -v mongodbdata:/data/db mongo
```
OR
```bash
docker compose up
```

add nugget common
```bash
dotnet nuget add source <dir nuget package common> -n PlayEconomy
```
```bash
dotnet add package Play.Cummon
```